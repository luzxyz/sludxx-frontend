FROM node:lts-alpine

WORKDIR /app

RUN apk update
RUN apk upgrade

COPY package.json .

RUN yarn install

COPY . .

RUN yarn build:ssr

EXPOSE 4000

ENTRYPOINT [ "yarn" ]
CMD [ "serve:ssr" ]
